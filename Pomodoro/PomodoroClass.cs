﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pomodoro {
    class PomodoroClass {

        public int Brojac { get; set; }

        public int UkupanBrojSekundi { get; set; }

        public int Minute()
        {
            return UkupanBrojSekundi / 60;
        }

        public int Sekunde()
        {
            return UkupanBrojSekundi;
        }

        public int PretvaranjeRada(string tekst)
        {
            UkupanBrojSekundi = int.Parse(tekst) * 60;
            return UkupanBrojSekundi;
        }       
    }
}
