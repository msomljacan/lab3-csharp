﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pomodoro {
    public partial class Form1 : Form {
        PomodoroClass Pomodoro;

        public Form1() {
            InitializeComponent();
            Pomodoro = new PomodoroClass();
            Pomodoro.PretvaranjeRada(tbRad.Text);
            Pomodoro.Brojac = 1;
        }

        private void btnStartStop_Click(object sender, EventArgs e)
        {
            tmrOdbrojavanje.Enabled = !tmrOdbrojavanje.Enabled;
        }

        private void btnReset_Click(object sender, EventArgs e) {
            lblVrijeme.Text = tbRad.Text;
            Pomodoro.PretvaranjeRada(tbRad.Text);
            lblVrijeme.Text = string.Format("{0:D2}:{1:D2}", Pomodoro.Minute(), Pomodoro.Sekunde());

            tmrOdbrojavanje.Enabled = false;
        }

        private void tmrOdbrojavanje_Tick(object sender, EventArgs e)
        {
            Text = "[" + string.Format("{0:D2}:{1:D2}", Pomodoro.Minute(), Pomodoro.Sekunde()) + "] Pomodoro App";

            Pomodoro.UkupanBrojSekundi--;
            lblVrijeme.Text = string.Format("{0:D2}:{1:D2}", Pomodoro.Minute(), Pomodoro.Sekunde());

            if(Pomodoro.UkupanBrojSekundi==0 && Pomodoro.Brojac %2 != 0)
            {
                Pomodoro.UkupanBrojSekundi--;
                Pomodoro.PretvaranjeRada(tbOdmor.Text);
                Pomodoro.Brojac++;
            }
            if(Pomodoro.UkupanBrojSekundi == 0 && Pomodoro.Brojac % 2 == 0)
            {
                Pomodoro.UkupanBrojSekundi--;
                Pomodoro.PretvaranjeRada(tbRad.Text);
                Pomodoro.Brojac++;
            }

        }
    }
}
