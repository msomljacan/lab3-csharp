﻿namespace Pomodoro
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tbRad = new System.Windows.Forms.TextBox();
            this.tbOdmor = new System.Windows.Forms.TextBox();
            this.lblRad = new System.Windows.Forms.Label();
            this.lblOdmor = new System.Windows.Forms.Label();
            this.lblVrijeme = new System.Windows.Forms.Label();
            this.btnStartStop = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.tmrOdbrojavanje = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // tbRad
            // 
            this.tbRad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbRad.Location = new System.Drawing.Point(94, 113);
            this.tbRad.Name = "tbRad";
            this.tbRad.Size = new System.Drawing.Size(203, 26);
            this.tbRad.TabIndex = 0;
            this.tbRad.Text = "25";
            // 
            // tbOdmor
            // 
            this.tbOdmor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbOdmor.Location = new System.Drawing.Point(498, 113);
            this.tbOdmor.Name = "tbOdmor";
            this.tbOdmor.Size = new System.Drawing.Size(203, 26);
            this.tbOdmor.TabIndex = 1;
            this.tbOdmor.Text = "5";
            // 
            // lblRad
            // 
            this.lblRad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRad.Location = new System.Drawing.Point(171, 89);
            this.lblRad.Name = "lblRad";
            this.lblRad.Size = new System.Drawing.Size(100, 23);
            this.lblRad.TabIndex = 2;
            this.lblRad.Text = "Rad";
            // 
            // lblOdmor
            // 
            this.lblOdmor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOdmor.Location = new System.Drawing.Point(572, 89);
            this.lblOdmor.Name = "lblOdmor";
            this.lblOdmor.Size = new System.Drawing.Size(83, 18);
            this.lblOdmor.TabIndex = 3;
            this.lblOdmor.Text = "Odmor";
            // 
            // lblVrijeme
            // 
            this.lblVrijeme.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVrijeme.Location = new System.Drawing.Point(359, 209);
            this.lblVrijeme.Name = "lblVrijeme";
            this.lblVrijeme.Size = new System.Drawing.Size(161, 70);
            this.lblVrijeme.TabIndex = 4;
            this.lblVrijeme.Text = "25:00";
            // 
            // btnStartStop
            // 
            this.btnStartStop.Location = new System.Drawing.Point(94, 312);
            this.btnStartStop.Name = "btnStartStop";
            this.btnStartStop.Size = new System.Drawing.Size(203, 72);
            this.btnStartStop.TabIndex = 5;
            this.btnStartStop.Text = "Start / Stop";
            this.btnStartStop.UseVisualStyleBackColor = true;
            this.btnStartStop.Click += new System.EventHandler(this.btnStartStop_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(498, 312);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(203, 72);
            this.btnReset.TabIndex = 6;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // tmrOdbrojavanje
            // 
            this.tmrOdbrojavanje.Interval = 10;
            this.tmrOdbrojavanje.Tick += new System.EventHandler(this.tmrOdbrojavanje_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnStartStop);
            this.Controls.Add(this.lblVrijeme);
            this.Controls.Add(this.lblOdmor);
            this.Controls.Add(this.lblRad);
            this.Controls.Add(this.tbOdmor);
            this.Controls.Add(this.tbRad);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblRad;
        private System.Windows.Forms.Label lblOdmor;
        public System.Windows.Forms.TextBox tbRad;
        public System.Windows.Forms.TextBox tbOdmor;
        public System.Windows.Forms.Label lblVrijeme;
        public System.Windows.Forms.Button btnStartStop;
        public System.Windows.Forms.Button btnReset;
        public System.Windows.Forms.Timer tmrOdbrojavanje;
    }
}

